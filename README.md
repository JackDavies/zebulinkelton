# Development environment

## First-time Setup

### Linux Setup

#### Dependencies

* `git`
* `docker`
* `docker-compose`

### Windows Setup

NOTE: These instructions are intended for running a local development
environment from Powershell or Windows Command Prompt. Cygwin/Git Bash
will not currently work with the changes that have been made to the
Makefile for Windows, and they experienced issues with TTYs when
trying to interact with bash inside the Docker containers.

#### Dependencies

* `git`
    * Do NOT enable Windows symlinks support, as this will conflict with
      this project's approach to handling symlinks inside the Docker
      containers.
    * Enable the option to make `git` available on the Windows shell.
* Docker community edition for Windows
    * https://store.docker.com/editions/community/docker-ce-desktop-windows
    * This includes `docker` and `docker-compose`
* `make` for Windows
    * http://gnuwin32.sourceforge.net/packages/make.htm
    * Once installed, you will need to ensure `C:\Program Files
      (x86)\GnuWin32\bin` is added to your PATH, or you have an alias
      to execute `C:\Program Files (x86)\GnuWin32\bin\make.exe` when
      you call `make`.
    * For Powershell, you can create a profile file at
      `Documents\WindowsPowerShell\profile.ps1` with the alias:
      `new-item alias:make -value 'C:\Program Files
      (x86)\GnuWin32\bin\make.exe'`.
        * For the profile to be executed when powershell launches, you
          will also need to configure Windows to allow local scripts
          to be run: `Set-ExecutionPolicy RemoteSigned`
          (https://serverfault.com/q/31194)

#### Git Configuration

In order for various script and configuration files to work correctly
inside the Docker containers, they must have unix-style `lf`
line-endings instead of Windows/DOS `crlf` line-endings, and Windows
symlinks should not be used by Git.

To do this, you should enforce some configuration values when cloning
the repository:

```
git clone \
-c core.symlinks=false \
-c core.autocrlf=false \
-c core.eol=lf \
$REPO_URL
```

Note: While it is ideal to change the line-ending settings when
cloning the repository, it should be possible to set these settings on
an existing repository, as long as the checked-out files are updated
to have the new line-endings:

```
# First, ensure you have no uncommitted local changes in git
git status
# Reference: https://stackoverflow.com/a/13154031
git config core.symlinks false
git config core.autocrlf false
git config core.eol lf
git rm --cached -rf .
git reset
```

### Running the Site

Then you can start the site by running `make environment`,
which will make the following URLs available to you:

* helloworld:
    * `http://localhost:8000` is the site running over HTTP
    * `http://localhost:8100` is the site running over HTTPS
    * `http://localhost:8200` is the mailcatcher inbox (though emails
      are currently forwarded to the dev email address via mandrill)
* SecondSite:
    * `http://localhost:9000` is the site running over HTTP
    * `http://localhost:9100` is the site running over HTTPS
    * `http://localhost:9200` is the mailcatcher inbox (though emails
      are currently forwarded to the dev email address via mandrill)

You can stop the Docker containers by running `make stop`


## Easy commands

`make environment` will spin up a Docker-based dev environment for
you.

When the docker-compose containers are up and running you can run the
following commands in another terminal. Each of these commands will
only apply to a single site, so you will need to include `site=SITE`
in the invocation of the command:

* `make bash` will open up a bash terminal in the web server container
  (with drush, drupal console, composer, and psql installed).
* `make sudo-bash` will open up a bash terminal as root in the web
  server container (with drush, drupal console, composer, and psql
  installed).
* `make db_dump` will create a new database dump in the
  `docker/shared/` directory from your existing db.
* `make db_restore file=dump.sql.gz` will restore the database from
  the dump file at `docker/shared/dump.sql.gz`
* `make db_mysql` to log in to mysql
* `make files_restore file=dump.tar.gz` will extract the files from
  `docker/shared/dump.sql.gz` into the site's
  `web/sites/default/files` directory.
* `make compass` to automatically re-compile SCSS files when they change.
* `make compass-once` to force a one-off re-compilation of the SCSS files.
* `make address-checker-app-watch` to automatically re-build the AngularJS
  address-checker-app when it changes.
* `make address-checker-build` to perform a one-off build of the AngularJS
  address-checker-app.
* `make config-import` to import the `config/sync` directory into the
  database.
* `make config-export` to export the database config to the
  `config/sync` directory.
* `make config-save` to save configuration from the untracked
  `config/sync` directory to the `config/local` and
  `config/base` directories.
* `make config-load` to load configuration from the
  `config/local` and `config/base` directories into the
  untracked `config/sync` directory.

You can also view Apache logs in `docker/logs/`.

## Managing configs

This is important since there's a high chance of having config changes when
checking out other people's branches.

[Drupal 8 Configuration Management] (https://redmine.catalyst.net.nz/projects/callplus-website/wiki/Drupal_8_Configuration_Management)

Make sure to backup your db if you have some work in progress.

## Adding/Updating Drupal Modules and Core with Composer

See: https://github.com/drupal-composer/drupal-project

### How can I apply patches to downloaded modules?

If you need to apply patches (depending on the project being modified, a pull
request is often a better solution), you can do so with the
[composer-patches](https://github.com/cweagans/composer-patches) plugin.

To add a patch to drupal module foobar insert the patches section in the extra
section of composer.json:
```json
"extra": {
    "patches": {
        "drupal/foobar": {
            "Patch description": "URL to patch"
        }
    }
}
```
