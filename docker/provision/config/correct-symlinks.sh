#!/bin/bash
set -e

# On Windows, we need to ensure that all files which should be
# symlinks actually ARE symlinks. See:
# https://stackoverflow.com/a/5930443
symlink_paths=`git ls-files -s | awk '/120000/{print $4}'`
for symlink_path in $symlink_paths; do
    if [ ! -L "$symlink_path" ]; then
        # Git for Windows creates symlink files as text files that
        # contain the target path of the link.
        target=`cat $symlink_path`
        # See: https://stackoverflow.com/a/15137779
        symlink_dir=${symlink_path%/*}
        symlink_file=${symlink_path##*/}
        # Move into the directory of the symlink, remove the existing
        # txt file, create the symlink, and move back to the previous
        # directory.
        cd $symlink_dir
        rm $symlink_file
        ln -s $target $symlink_file
        cd -
    fi
done
