#!/bin/bash
set -e

echo "export DB_HOST=$DB_HOST" > /etc/environment

# Ensure settings.local.php is in place.
LOCAL_SETTINGS_PATH="/var/www/config/settings.local.php"
LOCAL_SETTINGS_EXAMPLE_PATH="/var/www/web/sites/example.settings.local.php"
if [ ! -f $LOCAL_SETTINGS_PATH ]; then
    cp $LOCAL_SETTINGS_EXAMPLE_PATH $LOCAL_SETTINGS_PATH
fi

# Ensure files directory exists with permissions that will allow
# apache to write to it.
FILES_PATH="/var/www/web/files"
if [ ! -d $FILES_PATH ]; then
    mkdir $FILES_PATH
fi
chmod -R 777 $FILES_PATH

# Change ownership of all project files (especially mounted volumes like config/sync)
chown -R $DOCKER_UID /var/www

# Start mailcatcher.
/bin/bash -l -c "mailcatcher --ip=0.0.0.0"

# Start apache
service apache2 start

# Keep the container running.
sleep infinity
