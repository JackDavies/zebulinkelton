dump_filename = $(site)-db-`date '+%Y%m%d-%H:%M:%S'`.sql.gz

apache_container = $(site)_apache2
db_container = $(site)_database

ifeq ($(site), zebulinkelton)
	site_config = /data/www/www.zebulinkelton.com	
endif

ifeq ($(OS),Windows_NT)

uid = 1000
gid = 1000
environment:
	cmd /V /C 'set DOCKER_UID=$(uid)&& docker-compose up -d --build'
# Replace placeholder symlink files with real symlinks in the Docker containers
#	docker exec --user $(uid):$(gid) zebulinkelton_apache2 /correct-symlinks.sh
#	docker exec --user $(uid):$(gid) angular5_apache2 /correct-symlinks.sh
# Tell git that the symlink files haven't changed.
#	for /f "tokens=4" %%a in ('"git ls-files -s | findstr "^120000""') do git update-index --assume-unchanged %%a
stop:
	cmd /V /C 'set DOCKER_UID=$(uid)&& docker-compose stop'
require_site:
	@IF "$(site)"=="" ECHO "Please specify a target site by appending 'site=SITE' to your command" && exit 1
require_db_dump:
	@IF "$(file)"=="" ECHO "Please specify a target file that exists in docker/shared: 'make db_restore file=dump.sql.gz'" && exit 1
	@IF NOT EXIST "docker/shared/$(file)" ECHO "Please specify a target file that exists in docker/shared: 'make db_restore file=dump.sql.gz'" && exit 1
require_files_dump:
	@IF "$(file)"=="" ECHO "Please specify a target file that exists in docker/shared: 'make files_restore file=dump.tar.gz'" && exit 1
	@IF NOT EXIST "docker/shared/$(file)" ECHO "Please specify a target file that exists in docker/shared: 'make files_restore file=dump.tar.gz'" && exit 1
compass: require_site
	start /B docker exec --user $(uid):$(gid) $(apache_container) /bin/bash -l -c "compass watch /var/www/web/themes/secondSite_multisite"
	start /B docker exec --user $(uid):$(gid) $(apache_container) /bin/bash -l -c "compass watch /var/www/web/themes/$(site)"
# This command shouldn't be necessary, as git should be configured to
# checkout files with unix-style lf line-endings (see README.md), but
# it is kept here as a helpful utility.
fix-line-endings: require_site
	docker exec --user $(uid):$(gid) $(apache_container) /bin/bash -c "find . | xargs dos2unix"
tester:
	cmd /V /C 'set DOCKER_UID=$(uid)&& docker-compose up -d --build'
else

uid = $(shell id -u)
gid = $(shell id -g)
environment:
	DOCKER_UID=$(uid) docker-compose up -d --build
stop:
	DOCKER_UID=$(uid) docker-compose stop
require_site:
	@if [ -z "$(site)" ]; then echo "Please specify a target site by appending 'site=SITE' to your command"; exit 1; fi
require_db_dump:
	@if [ ! -f "docker/shared/$(file)" ]; then echo "Please specify a target file that exists in docker/shared: 'make db_restore file=dump.sql.gz'"; exit 1; fi
require_files_dump:
	@if [ ! -f "docker/shared/$(file)" ]; then echo "Please specify a target file that exists in docker/shared: 'make files_restore file=dump.tar.gz'"; exit 1; fi
compass: require_site
	docker exec --user $(uid):$(gid) $(apache_container) /bin/bash -l -c "compass watch /var/www/web/themes/$(site)"

endif

bash: require_site
	docker exec -ti --user $(uid):$(gid) $(apache_container) /bin/bash
sudo-bash: require_site
	docker exec -ti $(apache_container) /bin/bash
db_dump: require_site
	docker exec --user $(uid):$(gid) $(db_container) /bin/bash -c 'mysqldump --add-drop-database --user=fooBar --password=fooBar fooBar | gzip -f > /shared/$(dump_filename)'
	@echo "Dump created in docker/shared/"
db_restore: require_site require_db_dump
	docker exec $(db_container) /bin/bash -c 'cat /shared/$(file) | gunzip | mysql --user=fooBar --password=fooBar fooBar'
db_mysql: require_site
	docker exec -ti --user $(uid):$(gid) $(db_container) /bin/bash -c 'mysql --user=fooBar --password=fooBar fooBar'
view-running:
	docker ps -a