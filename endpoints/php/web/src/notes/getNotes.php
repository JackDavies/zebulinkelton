<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/jsonp; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


$dir    = '/var/www/notes/*.md';

$files = glob($dir);
$return = array();
for ($i = 0; $i < sizeof($files); $i++) {
    $title = str_replace('/var/www/notes/', '', $files[$i]);
    $title = str_replace('.md', '', $title);
    $return[$i] = $title;
}


try{
    echo json_encode($return);
} catch(Exception $e){
    $res = array(status => 400, message => $e->getMessage());
    echo( json_encode($res));
}

?>