<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/jsonp; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


$dir    = '/var/www/notes/';
$trash    = '/var/www/notes/.trash';
$name = ($_GET["name"]);
$file = $dir . $name .'.md';


try{
    if (!file_exists($trash)) {
        mkdir($trash, 0777, true);
    }
    rename($file, $trash.'/'.$name.'.md');
    $res = array(status => 200, message =>'success');
    echo( json_encode($res));
} catch(Exception $e){
    $res = array(status => 400, message => $e->getMessage());
    echo( json_encode($res));
}

?>