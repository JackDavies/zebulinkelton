<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/jsonp; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


$dir    = '/var/www/notes/';


$name = ($_GET["name"]);
$file = $dir . $name .'.md';

try{
    echo json_encode(file_get_contents($file));
} catch(Exception $e){
    $res = array(status => 400, message => $e->getMessage());
    echo( json_encode($res));
}

?>