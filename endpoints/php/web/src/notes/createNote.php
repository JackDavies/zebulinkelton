<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/jsonp; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
//$enctype=”multipart/form-data”;
$dir    = '/var/www/notes/';

$post = json_decode(file_get_contents('php://input'));
//echo $post;
$name = $post->name;
$content = $post->content;

$file = $dir . $name .'.md';

$handle = fopen($file, 'w') or die('Cannot open file:  '.$file);

try{
    fwrite($handle, $content);
    fclose($handle);
    $res = array(status => 200, message =>'success');
    echo( json_encode($res));
} catch(Exception $e){
    $res = array(status => 400, message => $e->getMessage());
    echo( json_encode($res));
}


?>