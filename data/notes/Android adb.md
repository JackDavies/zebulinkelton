# Android adb!!!

## Helpful Commands
- ```adb install```

- ```adb uninstall nz.net.orcon.MyOrcon```
- ```adb uninstall nz.co.slingshot.mobile ```
- ```adb shell pm uninstall nz.net.orcon.MyOrcon```

## From Bash (adb shell)
adb shell
adb shell pm uninstall nz.net.orcon.MyOrcon
- ```am start -n com.package.name/com.package.name.ActivityName```
### example:
- ```am start -n nz.net.orcon.MyOrcon/nz.net.orcon.MyOrcon.MainActivity```


## Ref URL's
[https://stackoverflow.com/questions/4756451/how-to-install-an-apk-file-on-an-android-phone]

