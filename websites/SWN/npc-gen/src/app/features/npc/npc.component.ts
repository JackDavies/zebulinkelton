import { Component, OnInit } from '@angular/core';

import { Character } from '../../character';


import { AttributesService } from '../../services/attributes/attributes.service';
import { BackgroundsService } from '../../services/backgrounds/backgrounds.service';
import { ClassesService } from '../../services/classes/classes.service';
import { LevelUpService } from '../../services/levelup/levelup.service';
import { LootService } from '../../services/loot/loot.service';
import { SkillsService } from '../../services/skills/skills.service';
import { WeaponsService } from '../../services/weopons/weapons.service';
import { UtilsService } from '../../utils.service';
import { NameGenService} from "../../services/nameGen/name-gen.service";

@Component({
  selector: 'app-npc',
  templateUrl: './npc.component.html',
  styleUrls: ['./npc.component.css']
})
export class NpcComponent implements OnInit {
  public npcs: Array<Character> = [];
  public npc;
  public selectedNpc;
  public classList = this.ClassesService.classList;
  public selectedClass;

  public backgroundList = this.BackgroundsService.backgroundList;
  public selectedBackground;

  public objectKeys = Object.keys;

  constructor(
    private AttributesService : AttributesService,
    private BackgroundsService : BackgroundsService,
    private ClassesService : ClassesService,
    private LevelUpService : LevelUpService,
    private LootService : LootService,
    private SkillsService : SkillsService,
    private WeaponsService : WeaponsService,
    private UtilsService : UtilsService,
    private NameGenService : NameGenService
  ) { }

  ngOnInit() {
  }


  generate(){
    this.UtilsService = new UtilsService();
    this.SkillsService = new SkillsService();
    this.ClassesService = new ClassesService(this.SkillsService);
    this.BackgroundsService = new BackgroundsService(this.SkillsService);
    this.AttributesService = new AttributesService(this.UtilsService);
    this.WeaponsService = new WeaponsService();
    
    let character = new Character;

    const nameGen = this.NameGenService.nameAndSex;
    const npcClass = this.ClassesService.getClass(this.selectedClass);
    const background = this.BackgroundsService.getBackground(this.selectedBackground);
    const attributes = this.AttributesService.attributes;
    const skills = this.SkillsService.skills;


    character.name = nameGen['name'];
    character.level = 1;
    character.hp = this.UtilsService.rollDice(1, npcClass['hitDie']) + (attributes.constitution);
    character.gender = nameGen['gender'];
    character.class = npcClass['type'];
    character.attributes = attributes;
    character.savingThrows = this.getSavingThrowsTEMP(npcClass['throws']);
    character.skills = skills;
    character.weapons = this.WeaponsService.randomWeapon(0,6);
    character.credit = this.UtilsService.rollDice(1, 100);

    this.markClassSKills(npcClass['skills']);
    this.setBaseSkillLevels(npcClass['trainingPackage']);
    this.setBaseSkillLevels(background['skills']);

    this.selectedNpc = character;
    this.npcs.push(character);
  }

  markClassSKills(skillsArray){
    for(let i = 0; i < skillsArray.length; i++){
      let skillKey = skillsArray[i];
      this.SkillsService.skills[skillKey].classSkill = true;
    }
  }

  setBaseSkillLevels(skillsArray){
    for(let i = 0; i < skillsArray.length; i++){
      let skillKey = skillsArray[i];
      this.SkillsService.skills[skillKey].level ++;
      this.SkillsService.skills[skillKey].skillPoints = this.SkillsService.costCalc(this.SkillsService.skills[skillKey].level, this.SkillsService.skills[skillKey].classSkill);
    }
  }

  levelUp(){
    this.LevelUpService.leveUp(this.npc);
  }

  getSavingThrows(throws, level){
    let keys = Object.keys(throws);
    let selectedThrow;
    for(let i = 0; i < keys.length; i++){
      if(keys[i] > level ){
        selectedThrow = keys[i]
      }
    }
  }

  getSavingThrowsTEMP(throws){
    let keys = Object.keys(throws);
    let selectedThrow;
    return throws[keys[0]]
  }


}
