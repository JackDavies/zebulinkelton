export class Character {
  name: string;
  class: string;
  level: number;
  gender: string;
  hp: number;
  background: string;
  skills: object;
  items: Array<object>;
  attributes: object;
  savingThrows: object;
  weapons: object
  credit: number
}