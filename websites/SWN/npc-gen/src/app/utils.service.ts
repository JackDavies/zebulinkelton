import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }


  rollDice(times,dice){
    var roll = 0;
    var i = 0;
    times = times ? times : 1;
    while (i < times){
      roll += Math.floor(Math.random() * dice + 1)
      i++;
    }
    return roll
  }
}
