import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NpcComponent } from './features/npc/npc.component';

import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule }    from '@angular/forms';
import { HomeComponent } from './pages/home/home.component';

import  { HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    NpcComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
