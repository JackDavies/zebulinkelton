import { Component, OnInit } from '@angular/core';
import { NotesService }  from '../notes-service.service';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public notes;
  public currentNote;
  name:string = '';

  private alive: boolean = true;
  private dirtyFlag: string;

  constructor(
      private notesService: NotesService) {
      notesService.dirtyFlag$.takeWhile(() => this.alive).subscribe(
          val=> {
              this.dirtyFlag = val;
              if(val){
                  this.getNotes();
              }
          });
  }

  ngOnInit() {
      this.getNotes();
  }

  viewNote(name): void{
    this.currentNote = name;
  }

  getNotes(): void {
      this.notesService.getNotes().subscribe(res => {this.notes = res; this.notesService.setDirty(false)});
  }

  createNote(): void {
      this.notesService.createNote(this.name, '').subscribe(res => this.notes = res);
  }
}
