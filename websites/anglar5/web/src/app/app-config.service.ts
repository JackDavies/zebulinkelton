import { Injectable } from '@angular/core';

@Injectable()
export class AppConfigService {
	getConfig() {
		return{
			notesAPIUrl: 'http://localhost:3000/notes'
		}
	}
}
