import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { ViewNoteComponent }             from './view-note/view-note.component';
import { NotesService }             from './notes-service.service';

import { AppRoutingModule }     from './app-routing.module';
import { AppConfigService } from './app-config.service';

import { Remarkable } from 'Remarkable';

import {FlexLayoutModule} from '@angular/flex-layout';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        FlexLayoutModule
    ],
    declarations: [
        AppComponent,
        DashboardComponent,
        ViewNoteComponent,
        NavbarComponent,
    ],
    providers: [ NotesService, AppConfigService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }