import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { map } from 'rxjs/operators';

import { AppConfigService } from './app-config.service';

@Injectable()
export class NotesService {

	constructor(
		private appConfig: AppConfigService,
        private http: HttpClient,
	){}
    noteAPIUrl = this.appConfig.getConfig().notesAPIUrl;

    private dirtyFlag = new Subject<any>();
    dirtyFlag$ = this.dirtyFlag.asObservable();
    setDirty(val) {
        this.dirtyFlag.next(val);
    }

	getNotes() {
        return this.http.get(this.noteAPIUrl + '/getNotes.php').pipe(
            map(res  => res) // or any other operator
        );
	}

    getNote(name) {
        return this.http.get(this.noteAPIUrl + '/getNote.php?name='+name).pipe(
            map(res  => res) // or any other operator
        );
    }
    deleteNote(name) {
        return this.http.get(this.noteAPIUrl + '/deleteNote.php?name='+name).pipe(
            map(res  => res) // or any other operator
        );
    }
    createNote(name, content) {
        return this.http.post(this.noteAPIUrl + '/createNote.php', {name:name, content:content}).pipe(
            map(res  => res) // or any other operator
        );
    }

}