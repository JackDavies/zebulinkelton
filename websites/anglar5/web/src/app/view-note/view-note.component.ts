import { Component,  OnChanges, Input  } from '@angular/core';
import { NotesService }  from '../notes-service.service';
import 'rxjs/add/operator/takeWhile';


@Component({
  selector: 'app-view-note',
  templateUrl: './view-note.component.html',
  styleUrls: ['./view-note.component.css'],
  inputs:['name']
})

export class ViewNoteComponent implements  OnChanges {
  public note;
  public deleteConfirm;
  public mdRender;

  private alive: boolean = true;
  private dirtyFlag: string;

  @Input() name: string;
  constructor(
    private notesService: NotesService
  ) {
      notesService.dirtyFlag$.takeWhile(() => this.alive).subscribe(
          val=> {
              this.dirtyFlag = val;
          });
  }

  ngOnChanges(changes: any) {
      console.log(changes);
    this.getNote(this.name);
  }

  getNote(name): void{
      var Remarkable = require('remarkable');
      this.mdRender = new Remarkable('full',{
          html: true, // Enable HTML tags in source
          xhtmlOut: true, // Use '/' to close single tags (<br />)
          breaks: true, // Convert '\n' in paragraphs into <br>
          linkify: false, // Autoconvert URL-like text to links

          // Enable some language-neutral replacement + quotes beautification
          typographer: false,

          // Double + single quotes replacement pairs, when typographer enabled,
          // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
          quotes: '“”‘’',
          highlight: function (str, lang) {}
      });
      this.note = {};
      this.note.name = name;
      this.notesService.getNote(name).subscribe(
          res => {
              this.note.content = res;
              this.setHeight('editNote');
          }
      );

  }

  setHeight(id): void {
      var el = document.getElementById(id);
      setTimeout(function(){
          el.style.height = '-webkit-fill-available';
          el.style.height = el.scrollHeight +'px';
      },0);

  }


  updateNote(): void {
     this.notesService.createNote(this.name, this.note.content).subscribe(res => this.note = res);
  }

  deleteNote(): void {
      this.notesService.deleteNote(this.name).subscribe(res => this.notesService.setDirty(true) );
  }


}